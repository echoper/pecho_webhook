FROM python:3.6-alpine3.7

RUN apk add  --update \
        python3-dev \
        build-base \
        linux-headers

WORKDIR /pecho_webhook
COPY ./requirements.txt /pecho_webhook

RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

COPY . /pecho_webhook

CMD [ "uwsgi", "--http", ":5000", \
               "--master", \
               "--wsgi-file", "pecho_webhook/wsgi.py", \
               "--callable", "app" ]
