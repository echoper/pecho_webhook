import os
import requests

from flask import Flask
from flask_httpauth import HTTPTokenAuth


token_auth = HTTPTokenAuth('Bearer')


@token_auth.verify_token
def verify_token(token):
    if token == os.environ.get('PECHO_WEBHOOK_TOKEN', 'testing'):
        return True
    return False


app = Flask(__name__)


@app.route('/salt/<target>')
@token_auth.login_required
def post_salt_netapi(target):
    url = f'http://127.0.0.1:8000/hook/flask/job/{target}'
    return requests.post(url).content
