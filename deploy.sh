echo "###"
echo "Connecting to docker hub"
echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
echo "###"

echo "###"
echo "Building pecho_webhook"
docker build -t perecho/pecho_webhook:latest  -f dependencies.Dockerfile .
echo "###"

echo "###"
echo "Pushing pecho_webhook"
docker push perecho/pecho_webhook:latest
echo "###"
